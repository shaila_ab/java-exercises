# Allergies

Write a program that, given a person's allergy score, can tell them whether or not they're allergic to a given item, and their full list of allergies.

An allergy test produces a single numeric score which contains the
information about all the allergies the person has (that they were
tested for).

The list of items (and their value) that were tested are:

* eggs (1)
* peanuts (2)
* shellfish (4)
* strawberries (8)
* tomatoes (16)
* chocolate (32)
* pollen (64)
* cats (128)

So if Tom is allergic to peanuts and chocolate, he gets a score of 34.

Now, given just that score of 34, your program should be able to say:

- Whether Tom is allergic to any one of those allergens listed above.
- All the allergens Tom is allergic to.

Developer Notes:

 * Added comprehensive tests using the bit shift mechanism of the Java bit operator to take all possible scores into consideration.
 Technical Details

1.	Developed the allergy finder using Java Bit operators. This simplified the program much more as the values are bit shifted by one position. Hence an easy way to find if a person is allergic to any specific Allergen is by intersecting (& operator) the score with the list of values in the Allergen Enum. 
2.	When a match is found, it would return the same value as the one passed.
3.	Wrote a common method (getAllergiesList()) that returns the list of allergies. If the list size is greaterThan 0, then the person is allergic to something, and hence returning true.
4.	This method can also be directly called to find specific list of allergies the person is allergic to.
5.	Enhanced the test class by adding two most inclusive testcases that actually use the bit operator to simulate the possible allergens and expected allergens. This test can be run for any number from 0 to x, to get list of expected allergies from the AllergyFinder. 

 

