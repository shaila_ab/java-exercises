import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by shailabalaraddi on 8/4/16.
 */
public class Allergies {
    private int allergy;
    private List<Allergen> allergiesList = Arrays.asList(Allergen.values());
    private Allergen myAllergen;

    public Allergies(int score){
        this.allergy=score;
        for(Allergen n:Allergen.values()){
            if(score == n.getScore())
                myAllergen = n;
        }
    }

    public boolean isAllergicTo(Allergen allergen ){
        return  this.getAllergiesList().contains(allergen);
    }

    public List<Allergen> getAllergiesList(){
        List<Allergen> allergyList = new ArrayList<Allergen>();

        for(Allergen n:Allergen.values()){

            if ((this.allergy & n.getScore()) == n.getScore()){
                allergyList.add(n);
            }

        }
        return allergyList;
    }
}
