import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AllergiesTest {

    @Test
    public void findAllergiesListTest(){
        for(int i = 0 ; i < 1000; i++) {
            List<Allergen> expected = determinePossibleAllergies(i);
            List<Allergen> found = new Allergies(i).getAllergiesList();
            System.out.println("Score {" + i + "}, expected allergies " + expected + ", found allergies " + found);
            assertEquals(expected, found);
        }
    }

    @Test
    public void findGivenAllergyTest(){
        for(int i = 0 ; i < 256; i++) {
            List<Allergen> expected = determinePossibleAllergies(i);
            for(Allergen expAllergen:expected){
                System.out.println("Score {" + i + "}, isAllergic to allergies " + expAllergen );
                assertEquals(true, new Allergies(i).isAllergicTo(expAllergen));
                int newRandomScore = i-expAllergen.getScore();
                System.out.println("Now passingScore {" + newRandomScore + "}");
                assertEquals(false, new Allergies(newRandomScore).isAllergicTo(expAllergen));
            }

        }
    }

    public static List<Allergen> determinePossibleAllergies(int value) {
        List<Allergen> list = new ArrayList<Allergen>();
        if((value & 1) == 1) {
            list.add(Allergen.EGGS);
        }
        if((value >> 1 & 1) == 1) {
            list.add(Allergen.PEANUTS);
        }
        if((value >> 2 & 1) == 1) {
            list.add(Allergen.SHELLFISH);
        }
        if((value >> 3 & 1) == 1) {
            list.add(Allergen.STRAWBERRIES);
        }
        if((value >> 4 & 1) == 1) {
            list.add(Allergen.TOMATOES);
        }
        if((value >> 5 & 1) == 1) {
            list.add(Allergen.CHOCOLATE);
        }
        if((value >> 6 & 1) == 1) {
            list.add(Allergen.POLLEN);
        }
        if((value >> 7 & 1) == 1) {
            list.add(Allergen.CATS);
        }
        return list;
    }

    @Test
    public void noAllergiesMeansNotAllergicToAnything() {
        Allergies allergies = new Allergies(0);

        assertEquals(false, allergies.isAllergicTo(Allergen.EGGS));
        assertEquals(false, allergies.isAllergicTo(Allergen.PEANUTS));
        assertEquals(false, allergies.isAllergicTo(Allergen.STRAWBERRIES));
        assertEquals(false, allergies.isAllergicTo(Allergen.CATS));
    }

    @Test
    public void allergicToEggs() {
        Allergies allergies = new Allergies(1);

        assertEquals(true, allergies.isAllergicTo(Allergen.EGGS));
    }

    @Test
    public void allergicToPeanuts() {
        Allergies allergies = new Allergies(2);

        assertEquals(true, allergies.isAllergicTo(Allergen.PEANUTS));
    }

    @Test
    public void allergicToShellfish() {
        Allergies allergies = new Allergies(4);

        assertEquals(true, allergies.isAllergicTo(Allergen.SHELLFISH));
    }

    @Test
    public void allergicToShellfishAndPeanuts() {
        Allergies allergies = new Allergies(6);

        List<Allergen> expectedAllergens = Arrays.asList(new Allergen[]{
                Allergen.PEANUTS,
                Allergen.SHELLFISH
        });

        assertEquals(expectedAllergens, allergies.getAllergiesList());
    }

    @Test
    public void allergicToStrawberries() {
        Allergies allergies = new Allergies(8);

        assertEquals(true, allergies.isAllergicTo(Allergen.STRAWBERRIES));
    }

    @Test
    public void allergicToTomatoes() {
        Allergies allergies = new Allergies(16);

        assertEquals(true, allergies.isAllergicTo(Allergen.TOMATOES));
    }

    @Test
    public void allergicToChocolate() {
        Allergies allergies = new Allergies(32);

        assertEquals(true, allergies.isAllergicTo(Allergen.CHOCOLATE));
    }

    @Test
    public void allergicToPollen() {
        Allergies allergies = new Allergies(64);

        assertEquals(true, allergies.isAllergicTo(Allergen.POLLEN));
    }

    @Test
    public void allergicToCats() {
        Allergies allergies = new Allergies(128);

        assertEquals(true, allergies.isAllergicTo(Allergen.CATS));
    }

    @Test
    public void isAllergicToEggsInAdditionToOtherStuff() {
        Allergies allergies = new Allergies(5);

        assertEquals(true, allergies.isAllergicTo(Allergen.EGGS));
    }

    @Test
    public void noAllergies() {
        Allergies allergies = new Allergies(0);

        assertEquals(0, allergies.getAllergiesList().size());
    }

    @Test
    public void isAllergicToJustEggs() {
        Allergies allergies = new Allergies(1);
        List<Allergen> expectedAllergens = Arrays.asList(new Allergen[]{Allergen.EGGS});

        assertEquals(expectedAllergens, allergies.getAllergiesList());
    }

    @Test
    public void isAllergicToJustPeanuts() {
        Allergies allergies = new Allergies(2);
        List<Allergen> expectedAllergens = Arrays.asList(new Allergen[]{Allergen.PEANUTS});

        assertEquals(expectedAllergens, allergies.getAllergiesList());
    }

    @Test
    public void isAllergicToJustStrawberries() {
        Allergies allergies = new Allergies(8);
        List<Allergen> expectedAllergens = Arrays.asList(new Allergen[]{Allergen.STRAWBERRIES});

        assertEquals(expectedAllergens, allergies.getAllergiesList());
    }

    @Test
    public void isAllergicToEggsAndPeanuts() {
        Allergies allergies = new Allergies(3);
        List<Allergen> expectedAllergens = Arrays.asList(new Allergen[]{
                Allergen.EGGS,
                Allergen.PEANUTS
        });

        assertEquals(expectedAllergens, allergies.getAllergiesList());
    }

    @Test
    public void isAllergicToEggsAndShellfish() {
        Allergies allergies = new Allergies(5);
        List<Allergen> expectedAllergens = Arrays.asList(new Allergen[]{
                Allergen.EGGS,
                Allergen.SHELLFISH
        });

        assertEquals(expectedAllergens, allergies.getAllergiesList());
    }

    @Test
    public void isAllergicToLotsOfStuff() {
        Allergies allergies = new Allergies(248);
        List<Allergen> expectedAllergens = Arrays.asList(new Allergen[]{
                Allergen.STRAWBERRIES,
                Allergen.TOMATOES,
                Allergen.CHOCOLATE,
                Allergen.POLLEN,
                Allergen.CATS
        });

        assertEquals(expectedAllergens, allergies.getAllergiesList());
    }

    @Test
    public void isAllergicToEverything() {
        Allergies allergies = new Allergies(255);
        List<Allergen> expectedAllergens = Arrays.asList(new Allergen[]{
                Allergen.EGGS,
                Allergen.PEANUTS,
                Allergen.SHELLFISH,
                Allergen.STRAWBERRIES,
                Allergen.TOMATOES,
                Allergen.CHOCOLATE,
                Allergen.POLLEN,
                Allergen.CATS
        });

        assertEquals(expectedAllergens, allergies.getAllergiesList());
    }

    @Test
    public void ignoreNonAllergenScoreParts() {
        Allergies allergies = new Allergies(509);
        List<Allergen> expectedAllergens = Arrays.asList(new Allergen[]{
                Allergen.EGGS,
                Allergen.SHELLFISH,
                Allergen.STRAWBERRIES,
                Allergen.TOMATOES,
                Allergen.CHOCOLATE,
                Allergen.POLLEN,
                Allergen.CATS
        });

        assertEquals(expectedAllergens, allergies.getAllergiesList());
    }
}
