import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by shailabalaraddi on 8/7/16.
 */
public class PigLatin {

    private static final String APPEND_CHAR="ay";
    //matches
    private static final String VOWEL_PATTERN="^[aeiou]|yt|xr";
    //matches any consonants like k,ch,pr sequentially
    private static final String CONSONANT_PATTERN="^[^aeiou]+";
    //matches consonant sounds like qu or squ
    private static final String CONSONANT_SOUND="^qu|^[bcdfghjklmnpqrstvwxyz]qu";



    public static String translate(String input){


        StringBuffer output = new StringBuffer();

        String[] words = input.split("\\s");

        for (String word: words) {
            StringBuffer outWord = new StringBuffer();

            String outS = null;

            Matcher vowelMatcher = match(PigLatin.VOWEL_PATTERN, word);
            Matcher consSoundMatcher = match(PigLatin.CONSONANT_SOUND, word);
            Matcher consMatcher = match(PigLatin.CONSONANT_PATTERN, word);

            if (vowelMatcher.lookingAt()) {
                outWord.append(word).append(PigLatin.APPEND_CHAR);
                outS = outWord.toString();
            } else if (consSoundMatcher.lookingAt()) {
                outS = processWord(consSoundMatcher, word);
            } else if (consMatcher.lookingAt()) {
                outS = processWord(consMatcher, word);
            }

            output.append(outS).append(" ");
        }
        return output.toString().trim();

    }

    /*
    Generic Match method
     */
    public static Matcher match(String regex, String input){
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher mc =pattern.matcher(input);

        return mc;
    }
    /*
    Word processing method based on the Piglatin rules to append Consonant groups, once found.
     */
    public static String processWord(Matcher mc, String word) {
        StringBuffer outWord = new StringBuffer();
        StringBuffer consgrp = new StringBuffer();
        consgrp.append(mc.group());
        consgrp.append(PigLatin.APPEND_CHAR);
        outWord.append(word.substring(mc.end())).append(consgrp);
        return outWord.toString();
    }
}
